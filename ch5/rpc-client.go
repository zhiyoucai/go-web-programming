package ch5

import (
	"fmt"
	"log"
	"net/rpc"
)

type ArgsTwo struct {
	X, Y int
}

func Client() {
	client, err := rpc.DialHTTP("tcp", "127.0.0.1:8808")
	if err != nil {
		log.Fatal("HTTP Error", err)
	}
	x := 1
	y := 2
	args := ArgsTwo{x, y}
	var reply int
	err = client.Call("Algorithm.Sum", args, &reply)
	if err != nil {
		log.Fatal("Call Sum algorithm error:", err)
	}
	fmt.Printf("Algorithm 和为：%d + %d = %d\n", x, y, reply)
}
