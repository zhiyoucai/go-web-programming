package ch5

import (
	"fmt"
	"net/http"
	"net/rpc"
)

type Args struct {
	X, Y int
}

type Algorithm int

func (a *Algorithm) Sum(args *Args, reply *int) error {
	*reply = args.X + args.Y
	fmt.Println("Exec Sum", *reply)
	return nil
}

func Init() {
	algorithm := new(Algorithm)
	fmt.Println("Algorithm start", algorithm)
	rpc.Register(algorithm)
	rpc.HandleHTTP()
	err := http.ListenAndServe(":8808", nil)
	if err != nil {
		fmt.Println("err=========", err.Error())
	}
}
