# 接收和处理Web请求

## 1. 多路复用路由、处理函数和处理器

如何定位到Web请求的资源，是RESTful的关注对象。比如这样一个URL请求：

```bash
curl --location 'localhost:8080/user?name=yes
```

多路复用路由解析URL之后，会将请求分发给对应的处理器来处理。net/http包中提供一个默认的多路复用器DefaultServeMux。

一个完整的使用了DefaultServeMux的代码样例如handler1.go中。

该代码样例中还有处理器的定义方法。所谓处理器就是一段逻辑，需要自行定义接收到请求之后的逻辑。

要自行定义处理器需要实现Handler接口，这里的片段如下：

```go
type handler1 struct{}

func (h1 *handler1) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi handler1")
}
```

实现之后就可以将处理器注册到多路复用器上了，逻辑如下：

```go
http.Handle("/handler1", &handler1)
```

不过这里用的是DefaultServeMux还无法看的很清楚注册的逻辑，因此在handler3.go中也实现了这样一段类似的逻辑，用于说明自定义处理器是如何工作的：

```go
type welcomeHandler struct {
	Name string
}

func (h *welcomeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is welcome, param is :%s", h.Name)
}
// 省略多路复用器的定义过程
mux.Handle("/welcome", &welcomeHandler{Name: "Good!"})
```

除了多路复用器之外还有多路复用函数，稍微简单一些不用实现接口：

```go
func webHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is webHandler")
}
// 省略多路复用器的定义过程
mux.HandleFunc("/web", webHandler)
```

利用Postman构造请求，访问不同的URL路径，则会得到不同的结果。这种实现难度比起Java来确实简单很多，不需要依赖任何框架就可以手动写一个Web服务器出来。

## 2. 表单数据的处理

之前在写Java代码的时候，基本没有用过GET请求，任何接口都统一设计成了POST。因此需要了解什么叫做表单。

**表单的entType属性**

- application/x-www-form-urlencoded
- multipart/form-data
- text/plain

以handler4.go中的greetingHandler1方法为例，服务启动后会接收POST请求并打印。由于这里定义了Header的Content-Type是application/x-www-form-urlencoded，因此可以用Form或者PostForm来处理，区别是PostForm仅仅支持Post请求。