package ch3

import (
	"fmt"
	"net/http"
)

type handler1 struct{}

func (h1 *handler1) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi handler1")
}

type handler2 struct{}

func (h2 *handler2) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "hi handler2")
}

func Serve() {
	// 定义处理器
	handler1 := handler1{}
	handler2 := handler2{}

	// 使用默认的多路复用器，因此Handler是nil
	server := http.Server{
		Addr:    "0.0.0.0:8085",
		Handler: nil,
	}

	http.Handle("/handler1", &handler1)
	http.Handle("/handler2", &handler2)
	server.ListenAndServe()
}
