package ch3

import (
	"fmt"
	"log"
	"net/http"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is indexHandler")
}

func hiHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is hiHandler")
}

func webHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is webHandler")
}

type welcomeHandler struct {
	Name string
}

func (h *welcomeHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Handler is welcome, param is :%s", h.Name)
}

func Serve3() {
	mux := http.NewServeMux()
	//注册处理器函数
	mux.HandleFunc("/", indexHandler)
	mux.HandleFunc("/hi", hiHandler)
	mux.HandleFunc("/web", webHandler)

	mux.Handle("/welcome", &welcomeHandler{Name: "Good!"})

	server := &http.Server{
		Addr:    ":8081",
		Handler: mux,
	}

	if err := server.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
