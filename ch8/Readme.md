# Web和Gin的使用

做Java程序的时候，可以利用Spring很容易的实现一套RESTful接口，从实现难度上来看，Golang似乎更加轻快。

正常情况下我还是习惯用框架来做这些事情，尽管有资料说Golang可以自己实现，但是框架的好处就是高度的集成，不用做许多重复工作。

以一个简单的HelloWorld为例，许多框架的最开始都是利用一个GET请求，在浏览器上打印一个hello world。

```go
func helloWorld() {
	r := gin.Default()
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(200, gin.H {
			"message": "Hello World!",
		})
	})
	r.Run()
}
```

平时使用的时候，更多的场景应该是和百度的搜索一样，要读取参数的，因此改动了一下代码：

```go
func QueryWithParam() {
	r := gin.Default()
	r.GET("/user", func(c *gin.Context) {
		name := c.Query("name")
		c.String(http.StatusOK, "Hello %s", name)
	})
	r.Run(":8080")
}
```

对应的GET请求就是`curl --location 'localhost:8080/user?name=yes`

还有一种常见的使用场景就是客户端传入一个请求体，里面是比较复杂的信息。对于Java来说就是client端将一个类的对象传给service。对于Golang来说就是struct了，因此改造一下代码：

```go
type Student struct {
	Uid int `json:"uid" form:"uid"`
	Name string `json:"name" form:"name"`
}

func PostWithParam() {
	r := gin.Default()
	r.POST("/user", func(c *gin.Context) {
		s := Student{}
		if c.ShouldBind(&s) == nil {
			fmt.Print(s.Name)
			fmt.Print(s.Uid)
		}
		c.String(200, "Success")
	})
	r.Run(":8080")
}
```

绑定了一个结构体，直接打印传入的参数。

掌握了这些基本的操作之后，后面做CRUD也就没什么太难的了。