package ch8

import (
	"fmt"
	"net/http"

	"github.com/gin-gonic/gin"
)

// helloWorld简单的打印Hello World
func helloWorld() {
	r := gin.Default()
	r.GET("/hello", func(c *gin.Context) {
		c.JSON(200, gin.H {
			"message": "Hello World!",
		})
	})
	r.Run()
}

// QueryWithParam 一般来说，curl --location 'localhost:8080/user?name=yes'更为常见
// 用Query则能读取到输入参数
func QueryWithParam() {
	r := gin.Default()
	r.GET("/user", func(c *gin.Context) {
		name := c.Query("name")
		c.String(http.StatusOK, "Hello %s", name)
	})
	r.Run(":8080")
}

type Student struct {
	Uid int `json:"uid" form:"uid"`
	Name string `json:"name" form:"name"`
}


// PostWithParam 可以用curl --location 'localhost:8080/user/' \
// --header 'Content-Type: application/json' \
// --data '{
//    "uid":123,
//    "name":"Tom"
//}' 命令进行测试
func PostWithParam() {
	r := gin.Default()
	r.POST("/user", func(c *gin.Context) {
		s := Student{}
		if c.ShouldBind(&s) == nil {
			fmt.Print(s.Name)
			fmt.Print(s.Uid)
		}
		c.String(200, "Success")
	})
	r.Run(":8080")
}