package ch7

func IntegerGenerator() chan int {
	ch := make(chan int)

	go func() {
		for i := 0; ; i++ {
			ch <- i
		}
	}()
	return ch
}
