package ch7

import (
	"fmt"
	"testing"
)

func TestRecombination(t *testing.T) {
	fmt.Println("------------------------------------------")
	result := Recombination(branch(10), branch(20), branch(30))
	for i := 0; i < 3; i++ {
		fmt.Println(<-result)
	}
}

func TestRecombination1(t *testing.T) {
	fmt.Println("------------------------------------------")
	result := Recombination(branch(10), branch(20), branch(30))
	for i := 0; i < 3; i++ {
		fmt.Println(<-result)
	}
}
