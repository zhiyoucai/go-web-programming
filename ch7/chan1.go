package ch7

import "fmt"

func Chan1() {
	ch := make(chan string)

	go func() {
		fmt.Println("开始goroutine")
		ch <- "signal"
		fmt.Println("退出goroutine")
	}()
	fmt.Println("等待goroutine")
	// 阻塞式通道，利用通道来实现goroutine之间的协同等待
	<-ch
	fmt.Println("完成")
}

// 没有通道进行协同，因此这里的输出是乱序的，正常情况下会先执行主goroutine中的打印输出
func Chan2() {
	go func() {
		fmt.Println("开始goroutine")
		fmt.Println("退出goroutine")
	}()
	fmt.Println("等待goroutine?")
	fmt.Println("完成")
}

// 构建一个长度为3的缓冲队列，是不会阻塞的
func Chan3() {
	ch1 := make(chan int, 3)
	go func() {
		fmt.Println("开始goroutine")
		for i := 0; i < 6; i++ {
			ch1 <- i
		}
		fmt.Println("退出goroutine")
	}()
	fmt.Println("等待goroutine?")
	<- ch1
	fmt.Println("完成")
}