package ch7

import (
	"fmt"
	"testing"
)

func TestIntegerGenerator(t *testing.T) {
	generator := IntegerGenerator()
	for i := 0; i < 100; i++ {
		fmt.Println(<-generator)
	}
}
