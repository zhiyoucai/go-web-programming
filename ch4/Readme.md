# 用Go连接MySQL并执行CRUD

MySQL需要做的准备工作是新建一个可以远程登录的用户，并且新建一个数据库以及一张表，新建数据库的过程就省略掉了，用Docker最好，下面是表的准备：

```sql
create database ch4;

use ch4;

CREATE TABLE `USER` (
  `uid` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`uid`)
);
```

在代码路径下执行这个命令来获取MySQL的驱动：

```bash
go get -u github.com/go-sql-driver/mysql
go mod tidy
```

Go语言的database/sql包里提供了Open函数，如下：

```go
func Open(driverName, dataSourceName string) (*DB, error)
```

和Java类似，需要提供驱动的名称和URL连接串就可以，下面代码片段就是使用样例：

```go
db, err = sql.Open("mysql", "root:a123456!@tcp(localhost:3306)/ch4")
```

正常的生产环境里没人直接写SQL代码，都是用ORM框架，这里根据工作原因选取了GORM框架。

```bash
go get -u gorm.io/gorm
go mod tidy
```

代码写在orm.go中。