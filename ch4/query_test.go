package ch4

import (
	// "fmt"
	"testing"
)

func TestQueryManyRows(t *testing.T) {
	initDB()
	queryManyRows(1)
}

func TestQueryRow(t *testing.T) {
	initDB()
	id := 1
	queryRow(id)
}

func TestInsertRows(t *testing.T) {
	initDB()
	var users = []User{{0, "Zhao", "11233"}, {0, "Wang", "99808"}}
	insertRows(users)
}

func TestUpdateRows(t *testing.T) {
	initDB()
	user := User{1, "zhiquan", "88888"}
	updateRows(user)
}

func TestPrepareQuery(t *testing.T) {
	initDB()
	prepareQuery(1)
}

func TestInsertWithTrans(t *testing.T) {
	initDB()
	var user1 = User{0, "Peter", "891821"}
	insertWithTrans(user1)
}
