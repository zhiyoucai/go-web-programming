package ch4

type User struct {
	Uid   int
	Name  string
	Phone string
}

func (User) TableName() string {
	return "USER"
}
