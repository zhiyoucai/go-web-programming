package ch4

import (
	"fmt"

	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var (
	client *gorm.DB
)

func initClient() (err error) {
	dsn := "root:a123456!@tcp(localhost:3306)/ch4"
	client, err = gorm.Open(mysql.Open(dsn), &gorm.Config{})
	if err != nil {
		return err
	}
	return nil
}

func queryOneRow(id int) {
	var user User
	client.First(&user, id)

	fmt.Printf("user name = %s\n", user.Name)

}
